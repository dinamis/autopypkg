# -*- coding: utf-8 -*-
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="autopypkg",
    version="1.0.1",
    author="Remi Cresson",
    packages=setuptools.find_packages(),
    python_requires=">=3.8",
    install_requires=["gitpython", "requests"],
)

