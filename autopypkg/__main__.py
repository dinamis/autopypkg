import os
from git import Repo, Actor
import requests
import argparse
import subprocess
import tempfile
import datetime

parser = argparse.ArgumentParser(description="AutoPyPkg", )
parser.add_argument("--trigger_ids", nargs='+', default=[])
parser.add_argument("--trigger_tokens", nargs='+', default=[])
parser.add_argument("--ref", default="main", help="Process only commits on this ref")
params = parser.parse_args()


def get_envvar(key):
    assert key in os.environ, f"Environment variable {key} not defined!"
    return os.environ[key]


COMMIT_MESSAGE = "[AutoPyPkg] Bump version"
commit_msg = get_envvar("CI_COMMIT_MESSAGE")
repo_dir = tempfile.mkdtemp()
url = get_envvar("CI_REPOSITORY_URL")
url = url.replace(get_envvar("CI_JOB_TOKEN"), get_envvar("ACCESS_TOKEN"))
url = url.replace("gitlab-ci-token", f"project_{get_envvar('CI_PROJECT_NAME')}_bot")
repo = Repo.clone_from(url, repo_dir)
repo.config_writer().set_value("user", "name", "AutoPyPkg").release()
repo.config_writer().set_value("user", "email", "noreply@gitlab-irstea.fr").release()

if get_envvar('CI_COMMIT_BRANCH') != params.ref:
    quit()

if commit_msg == COMMIT_MESSAGE:

    subprocess.run(["python", "-m", "build"])

    # Build package
    subprocess.run(["python",
                    "-m",
                    "twine",
                    "upload",
                    "--repository-url",
                    "{get_envvar('CI_API_V4_URL')}/projects/{get_envvar('CI_PROJECT_ID')}/packages/pypi",
                    "dist/*"],
                   env={"TWINE_PASSWORD": get_envvar('CI_JOB_TOKEN'),
                        "TWINE_USERNAME": "gitlab-ci-token"})

    for pid, ptoken in zip(params.trigger_ids, params.trigger_tokens):
        print(f"Trigger pipeline {pid}")
        requests.post(url=f"{get_envvar('CI_API_V4_URL')}/projects/{i}/trigger/pipeline",
                      data={"token": ptoken, "ref": "main", })
    quit()


# Search setup.py
def search_setup():
    for root, dirs, files in os.walk(repo_dir):
        if "setup.py" in files:
            return os.path.join(root, "setup.py")
    raise FileNotFoundError("Unable to find setup.py")


setup_py = search_setup()

# Read in the file
with open(setup_py, 'r') as file:
    filedata = file.read()

# Replace the target string
print(filedata)
kw = "version="
kwloc = filedata.find(kw)
suffix = filedata[kwloc + len(kw):]
sep = suffix[0]
splits = suffix.split(sep)
assert len(splits) > 0, "Version must be wrote directly after \"version=\""
version = splits[1]
version_splits = version.split(".")
assert all(i.isnumeric() for i in version_splits), "Version must be in the form Major.Minor.Patch"
print(f"Found current version {version}")
version_splits = [int(i) for i in version_splits]
version_splits[2] += 1  # Update patch
new_version = ".".join([str(i) for i in version_splits])
print(f"New version {new_version}")
filedata = filedata.replace(f"{sep}{version}{sep}", f"{sep}{new_version}{sep}")
print(filedata)

# Write the file out again
with open(setup_py, 'w') as file:
    file.write(filedata)

# Update git
repo.git.add(update=True)
setup_py = setup_py.replace(repo_dir + "/", "")
print(f"Adding file to git: {setup_py}")
repo.index.add([setup_py])
repo.index.commit(COMMIT_MESSAGE)
origin = repo.remote(name='origin')
origin.push()