# AutoPyPkg

Automatic python packager for gitlab CI

## Usage

1. Create an access token (**Settings** > **Access tokens** and tick *read_repository* and *write_repository*)
2. Copy the access token. Create a masked variable (**Settings** > **CI/CD** > **Variables** (expand) > **Add variable**). Name it `ACCESS_TOKEN`.
3. Add the following in the `.gitlab-ci.yml`:
```
image: python:latest

run:
  script:
    - pip install .
    - python -m autopypkg
```

## Trigger pipeline

```
python -m autopypkg --trigger_ids 3564 --trigger_tokens $SOME_TRIGGER_TOKEN
```
